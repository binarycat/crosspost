# crosspost

config file location: `~/.config/crosspostrc.toml`

# example configuration
```toml
# name of the category to post to by default ("all" to post to everything)
to = default

[[acct]]
nick = "my-twtxt"
proto = "twtxt"
cat = ["default", "tech"]
# the "twtxt" protocol appends to a file using the twtxt format
file = "/mnt/sshfs/personal_website/twtxt.txt"

[[acct]]
nick = "fedi-dev"
proto = "mastodon"
# categories this account belongs to
cat = ["tech"]
url = "https://mastodon.example/"
# currently, you need to get an oauth token manually (see https://docs.joinmastodon.org/client/token/)
token = OAUTH_TOKEN

[[acct]]
nick = "bs"
proto = "bsky"
# bluesky uses app passwords, not oauth
username = "BLUESKY_USERNAME.bsky.social"
password = "BLUESKY_APP_PASSWORD"
```

# example usage
`crosspost --to tech "hey check this out"`
