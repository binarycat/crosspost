mod mastodon;
mod twtxt;
mod bsky;

use crate::common::Proto;

pub const LIST: [&'static dyn Proto; 3] = [
	&mastodon::Mastodon,
	&twtxt::Twtxt,
	&bsky::Bluesky,
];
