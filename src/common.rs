use crate::conf::Account;
use time::{OffsetDateTime};
use time::format_description::well_known::Rfc3339;

#[derive(Default)]
pub struct Post {
	pub body: String,
}

#[derive(Debug)]
pub enum Status {
	Ok,
	Err(String),
}

pub trait Proto {
	fn name(&self) -> &'static str;
	fn publish(&self, acct: &Account, post: &Post) -> Status;
}

/// generate an rfc 3339 timestamp for the current moment.
pub fn timestamp() -> String { OffsetDateTime::now_utc().format(&Rfc3339)
							   .unwrap() }
