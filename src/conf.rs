use toml;
use serde::{Deserialize};
use std::fs;

/// all fields optional unless specified.
/// some optional fields may be required for certain protocols
#[derive(Deserialize,Debug)]
pub struct Account {
	/// (required) used to identify this account locally
	pub nick: String,
	/// (required) protocol to use for this account
	pub proto: String,
	/// categories, if you specify posting to a category name, it posts to all accounts with that category
	#[serde(default)]
	pub cat: Vec<String>,
	/// oauth token used for authentication
	#[serde(default)]
	pub token: String,
	/// api endpoint
	#[serde(default)]
	pub url: String,
	/// filepath, mainly used for local twtxt files
	#[serde(default)]
	pub file: String,
	/// if true, posts will not go to this account
	#[serde(default)]
	pub disabled: bool,
	#[serde(default, alias = "user")]
	pub username: String,
	#[serde(default, alias = "pass")]
	pub password: String,
}

impl Account {
	pub fn is_match(&self, s: &str) -> bool {
		if self.disabled { return false; }
		if s == "all" { return true; }
		return self.nick == s || self.cat.contains(&s.to_string());
	}
}

#[derive(Deserialize)]
pub struct Config {
	/// default value for the --to flag
	pub to: Option<String>,
	/// list of accounts
	pub acct: Vec<Account>,
}

impl Config {
	pub fn load(filename: &str) -> Self {
		let cfg: Config = toml::from_str(&fs::read_to_string(filename).expect("unable to read config file")).expect("unable to parse config file");
		return cfg
	}
}
