//! program for posting to multiple accounts at once

mod common;
mod proto;
mod conf;

use conf::{Config, Account};
use common::{Post, Status};
use std::env;
use std::path::Path;

fn post_to(acct: &Account, post: &Post) -> Status {
	for prot in proto::LIST.iter() {
		if acct.proto == prot.name() {
			//println!{"posting to {} ({})", acct.nick, acct.proto};
			return prot.publish(acct, post);
		}
	}
	panic!("unknown protocol '{}'", acct.proto);
}

fn find_config() -> String {
	// TODO: use XDG_CONFIG_HOME if present, also handle windows.
	let cfg_path = Path::new(&env::var("HOME").unwrap())
		.join(".config/crosspostrc.toml");
	return format!{"{}", cfg_path.as_path().display()};
}

fn main() {
	let mut post_body: Option<String> = None;
	let mut arg_to: Option<String> = None;
	let mut arg_mode: isize = 0;
	for arg in std::env::args() {
		if arg_mode > 0 {
			if arg_mode == 1 {
				arg_to = Some(arg.to_string());
			}
			arg_mode = 0;
		} else if arg.starts_with("--") {
			match &arg[2..] {
				"to" => arg_mode = 1,
				_ => panic!("unknown flag: {}", arg)
			}
		} else {
			post_body = Some(arg.to_string());
		}
	}
    let cfg = Config::load(&find_config());
	let to = arg_to.or(cfg.to).unwrap_or("all".to_string());
	let post = Post{
		body: post_body.expect("post body not specified").to_string()
	};
	for acct in cfg.acct {
		if acct.is_match(&to) {
			let status = post_to(&acct, &post);
			println!{"posted to {}: {:?}", acct.nick, status};
		}
	};
}
