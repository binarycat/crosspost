use crate::common::{Post, Proto, Status, timestamp};
use crate::conf::Account;
use json::{object};
use reqwest;

pub struct Bluesky;

impl Proto for Bluesky {
	fn name(&self) -> &'static str { "bsky" }
	fn publish(&self, a: &Account, p: &Post) -> Status {
		let cli = reqwest::blocking::Client::new();
		let auth_r = cli
			.post("https://bsky.social/xrpc/com.atproto.server.createSession")
			.header("Content-Type", "application/json")
			.body(object!{
				identifier: a.username.clone(),
				password: a.password.clone(),
			}.dump()).send().expect("http auth request failed");
		let auth_obj = match json::parse(&auth_r.text().unwrap()) {
			Ok(s) => s,
			Err(_) =>
				return Status::Err("failed to parse json response".to_string()),
		};
		let token = auth_obj["accessJwt"].as_str()
			.expect("returned json does not contain token").to_string();
		let post_r = cli
			.post("https://bsky.social/xrpc/com.atproto.repo.createRecord")
			.header("Content-Type", "application/json")
			.bearer_auth(&token)
			.body(object!{
				repo: a.username.clone(),
				collection: "app.bsky.feed.post",
				record: object!{
					text: p.body.clone(),
					createdAt: timestamp(),
				},
			}.dump()).send().expect("http post request failed");
		if post_r.status() == 200 {
			return Status::Ok;
		}
		return Status::Err(post_r.text().unwrap());
	}
}
