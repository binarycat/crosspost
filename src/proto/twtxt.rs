use crate::common::{Post, Proto, Status, timestamp};
use crate::conf::Account;
use std::fs::OpenOptions;
use std::io::Write;


/// protocol for local twtxt file (or it can be mounted via sshfs)
pub struct Twtxt;

impl Proto for Twtxt {
	fn name(&self) -> &'static str { "twtxt" }
	fn publish(&self, a: &Account, p: &Post) -> Status {
		let mut file = OpenOptions::new()
			.write(true)
			.append(true)
			.open(&a.file).expect("unable to open file!");
		let ts = timestamp();
		if let Err(e) = write!(file, "{}\t{}\n", ts, p.body) {
			return Status::Err(e.to_string());
		}
		return Status::Ok;
	}
}
