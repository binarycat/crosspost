use crate::common::{Post, Proto, Status};
use crate::conf::Account;
use json::{object};
use reqwest;

pub struct Mastodon;


impl Proto for Mastodon {
	fn name(&self) -> &'static str { "mastodon" }
	fn publish(&self, a: &Account, p: &Post) -> Status {
		let cli = reqwest::blocking::Client::new();
		// TODO: properly parse url and do a relative resolve
		let resp = cli.post(a.url.clone()+"api/v1/statuses")
			.bearer_auth(&a.token)
			.header("Content-Type", "application/json")
			.body(object!{
				status: p.body.clone(),
			}.dump()).send().expect("http request failed");
		// TODO: better error handling and checking!!
		if resp.status() == 200 {
			return Status::Ok;
		} else {
			return Status::Err(format!("{:?}",resp.text()));
		}
		//println!("got response: {:?}", resp);
		//return Status::OK;
	}
}


// TODO: some system to make obtaining oauth tokens easier and not require curl (maybe a shell script?)
